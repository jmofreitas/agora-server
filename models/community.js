const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const communitySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
    },
    number_members: {
        type: Number,
        default: 0
    },
    category: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    created_at: {type: Date, default: Date.now},
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}
});


module.exports = mongoose.model('Community', communitySchema);