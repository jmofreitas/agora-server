const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UpvotesHistorySchema = new Schema({
    user_rewarded: {
        type: String,
        required: true
    },
    user_rewarding: {
        type: String,
        required: true
    },
    comment: {
        type: String,
        default: null
    },
    topic: {
        type: String,
        default: null,
    },
    upvote:{
        type: Boolean,
        required: true,
        default: true
    },
    timestamp: {type: Date, default: Date.now},
});


module.exports = mongoose.model('UpvotesHistory', UpvotesHistorySchema);