const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const conversationSchema = new Schema({
    user_1: {
        type: String,
        required: true
    },
    user_2: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}
});


module.exports = mongoose.model('Conversation', conversationSchema);