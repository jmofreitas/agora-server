const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const givenBadgeSchema = new Schema({
    user: {
        type: String,
        required: true
    },
    comment: {
        type: String,
        default: null
    },
    topic: {
        type: String,
        default: null
    },
    badge: {
        type: String,
        required: true,
    },
    date_given: {type: Date, default: Date.now},
    attributed_by: {type: String, required: true},
    status: {type: Boolean, default: true}
});


module.exports = mongoose.model('GivenBadge', givenBadgeSchema);