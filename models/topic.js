const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const topicSchema = new Schema({
    user: {type: String, required: true},
    title: {type: String, required: true},
    body: {type: String, required: true},
    source: {type: String, default: null},
    metaImage: {
        type: String,
        default: null
    },
    category: {type: String, required: true},
    tags: {type: Array, default: []},
    public: {type: Boolean, default: true},
    upvotes: {type: Number, default: 0},
    nrComments: {type: Number, default: 0},
    main: {type: Boolean, required: true, default: false},
    community: {type: String, default: null},
    created_at: {type: Date, default: Date.now},
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}
});


module.exports = mongoose.model('Topic', topicSchema);