const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({
    parent: { type: String, default: null},
    topic: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required:true
    },
    user: {
        type: String,
        required: true
    },
    upvotes: {
        type: Number,
        required: true,
        default: 0
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}
});


module.exports = mongoose.model('Comment', commentSchema);