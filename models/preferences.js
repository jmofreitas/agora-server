const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const preferencesSchema = new Schema({
    category: {
        type: String,
        required: true
    },
    user: {
        type: String,
        required: true,
    },
    value: {
        type: Boolean,
        required: true,
    },
    created_at: {type: Date, default: Date.now},
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}
});


module.exports = mongoose.model('Preferences', preferencesSchema);