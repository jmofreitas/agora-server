const Joi = require('joi');
const mongoose = require('mongoose');

const User = mongoose.model('User', new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50
    },
    lastName: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 1024
    },
    role: {
        type: String,
        required: true,
        default: "5e336083cf4d187e1597f27b"
    },
    karma: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        default: null
    },
    userDescription: {
        type: String,
        maxlength: 1000,
        default: null
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}

}));

function validateUser(user) {
    const schema = {
        firstName: Joi.string().min(2).max(50).required(),
        lastName: Joi.string().min(2).max(50).required(),
        image: Joi.string().min(2),
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required(),
        userDescription: Joi.string().max(1000),
    };
    return Joi.validate(user, schema);
}

exports.User = User;
exports.validate = validateUser;

