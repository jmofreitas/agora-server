const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const favouriteSchema = new Schema({
    user: {
        type: String,
        required: true
    },
    topic:{
        type: String,
        required: true
    },
    created_at: {type: Date, default: Date.now},
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}
});


module.exports = mongoose.model('Favourite', favouriteSchema);