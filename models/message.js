const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const messageSchema = new Schema({
    from: {
        type: String,
        required: true,
    },
    to: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    conversation: {
        type: String,
        required: true,
    },
    timestamp: {
        type: Date,
        default: Date.now
    },
});


module.exports = mongoose.model('Message', messageSchema);