const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tagSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    category:{
        type: String,
        required: true
    },
    created_at: {type: Date, default: Date.now},
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}
});


module.exports = mongoose.model('Tag', tagSchema);