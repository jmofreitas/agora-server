const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const karmaSchema = new Schema({
    comments: {
        type: Number,
        default: 0
    },
    topics: {
        type: Number,
        default: 0
    }
});


module.exports = mongoose.model('Karma', karmaSchema);