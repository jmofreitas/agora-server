const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserCommunitySchema = new Schema({
    user: {
        type: String,
        required: true,
    },
    community: {
        type: String,
        required: true,
    },
    status: {
        type: Boolean,
        default: null
    },
    role: {
        type: String,
        default: '5e3360eecf4d187e1597f27d'
    },
    entry_message:{
        type: String,
        required: true
    },
    created_at: {type: Date, default: Date.now},
    edited_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null}
});


module.exports = mongoose.model('UserCommunity', UserCommunitySchema);