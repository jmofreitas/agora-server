const graphql = require('graphql');
const _ = require('lodash');
const Topic = require('../models/topic');
const Comment = require('../models/comment');
const Tag = require('../models/tag');
const Badge = require('../models/badge');
const GivenBadge = require('../models/givenbadge');
const Category = require('../models/category');
const Role = require('../models/role');
const Community = require('../models/community');
const Karma = require('../models/karma');
const Preferences = require('../models/preferences');
const Favourite = require('../models/favourite');
const UserCommunity = require('../models/usercommunity');
const Conversation = require('../models/conversation');
const Message = require('../models/message');
const UpvotesHistory = require('../models/upvoteshistory');
const User = require('../models/user').User;

const setUserValues = require('../recommendations/rec.js').setUserValues;
const addItem = require('../recommendations/rec.js').addItem;
const addDetail = require('../recommendations/rec.js').addDetail;
const recommendToUser = require('../recommendations/rec.js').recommendToUser;

const recombee = require('recombee-api-client');
const rqs = recombee.requests;

const client = new recombee.ApiClient('projectagora-prod', 'qG5yaOZODkdZisTt0KHio1VT1SUqLEcjjya3II4gFTUTLGurBuRLQiqPBvM6vAvX');


const {
    GraphQLObjectType,
    GraphQLInputObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLBoolean,
    GraphQLError
} = graphql;


//INPUT TYPES


const BadgesArray = new GraphQLInputObjectType({
    name: 'BadgesArray',
    fields: () => ({
        _id: {type: GraphQLID},
        name: {type: GraphQLString}
    })
});

const GivenBadgesInput = new GraphQLObjectType({
    name: 'BadgesGiven',
    fields: () => ({
        _id: {type: GraphQLID},
        //FALTA AQUI O USER TYPE
        user: {type: GraphQLID},
        comment: {type: GraphQLID},
        topic: {type: GraphQLID},
        badge: {type: GraphQLID},
        date_given: {type: GraphQLString},
        attributed_by: {type: GraphQLID},
        status: {type: GraphQLBoolean}
    })
});

const PreferencesInput = new GraphQLObjectType({
    name: 'PreferencesInput',
    fields: () => ({
        _id: {type: GraphQLID},
        user: {type: GraphQLID},
        category: {type: GraphQLID},
        value: {type: GraphQLBoolean}
    })
});

const UserCommunityInput = new GraphQLObjectType({
    name: 'UserCommunityInput',
    fields: () => ({
        _id: {type: GraphQLID},
        user: {type: GraphQLString},
        community: {type: GraphQLString},
        status: {type: GraphQLBoolean},
        role: {type: GraphQLString},
        entry_message: {type: GraphQLString},
    })
});

const TopicTypeInput = new GraphQLObjectType({
    name: 'TopicInput',
    fields: () => ({
        _id: {type: GraphQLID},
        title: {type: GraphQLString},
        body: {type: GraphQLString},
        user: {type: GraphQLID},
        source: {type: GraphQLString},
        metaImage: {type: GraphQLString},
        category: {type: GraphQLID},
        tags: {type: GraphQLList(TagsType)},
        upvotes: {type: GraphQLInt},
        nrComments: {type: GraphQLInt},
        main: {type: GraphQLBoolean},
        community: {type: GraphQLID},
        userRequesting: {type: GraphQLString}
    })
});

const TopicRecType = new GraphQLObjectType({
    name: 'TopicRec',
    fields: () => ({
        user: {type: GraphQLID},
        topic: {type: GraphQLID}
    })
});

const TopicTagInput = new GraphQLInputObjectType({
    name: 'TopicTagInput',
    fields: () => ({
        _id: {type: GraphQLID},
        name: {type: GraphQLString}
    })
});


//OUTPUT TYPES
const CategoryType = new GraphQLObjectType({
    name: 'Category',
    fields: () => ({
        _id: {type: GraphQLID},
        name: {type: GraphQLString},
        description: {type: GraphQLString},
        image: {type: GraphQLString},
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString}
    })
});

const BadgesType = new GraphQLObjectType({
    name: 'Badge',
    fields: () => ({
        _id: {type: GraphQLID},
        name: {type: GraphQLString},
        description: {type: GraphQLString},
        image: {type: GraphQLString},
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
    })
});

const UpvotesHistoryType = new GraphQLObjectType({
    name: 'UpvotesType',
    fields: () => ({
        _id: {type: GraphQLID},
        user_rewarded: {
            type: UserType,
            resolve(parent, args) {
                return User.findById(parent.user_rewarded)
            }
        },
        user_rewarding: {
            type: UserType,
            resolve(parent, args) {
                return User.findById(parent.user_rewarding)
            }
        },
        topic: {
            type: TopicType,
            resolve(parent, args) {
                return Topic.findById(parent.topic)
            }
        },
        upvote: {type: GraphQLBoolean}
    })
});


const FavouriteType = new GraphQLObjectType({
    name: 'FavouriteType',
    fields: () => ({
        _id: {type: GraphQLID},
        user: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({_id: parent.user})
            }
        },
        topic: {
            type: GraphQLList(TopicType),
            resolve(parent, args) {
                return Topic.find({_id: parent.topic})
            }
        },
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
    })
});


const RoleType = new GraphQLObjectType({
    name: 'Role',
    fields: () => ({
        _id: {type: GraphQLID},
        description: {type: GraphQLString},
        name: {type: GraphQLString}
    })
});

const ConversationType = new GraphQLObjectType({
    name: 'Conversation',
    fields: () => ({
        _id: {type: GraphQLID},
        user_1: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({_id: parent.user_1, deleted_at: null});
            }
        },
        user_2: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({_id: parent.user_2, deleted_at: null});
            }
        },
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
        message: {
            type: GraphQLList(MessageType),
            resolve(parent, args) {
                return Message.find({_conversation: parent._id}).sort({timestamp: -1}).limit(30);
            }
        }
    })
});

const MessagePages = new GraphQLObjectType({
    name: 'MessagePages',
    fields: () => ({
        _id: {type: GraphQLID},
        from: {type: GraphQLID},
        to: {type: GraphQLID},
        body: {type: GraphQLString},
        conversation: {type: GraphQLString},
        timestamp: {type: GraphQLString},
        skip: {type: GraphQLInt}
    })
});

const MessageType = new GraphQLObjectType({
    name: 'Message',
    fields: () => ({
        _id: {type: GraphQLID},
        from: {type: GraphQLID},
        to: {type: GraphQLID},
        body: {type: GraphQLString},
        conversation: {type: GraphQLString},
        timestamp: {type: GraphQLString}
    })
});

const KarmaType = new GraphQLObjectType({
    name: 'Karma',
    fields: () => ({
        _id: {type: GraphQLID},
        comments: {type: GraphQLInt},
        topics: {type: GraphQLInt}
    })
});

const PreferencesType = new GraphQLObjectType({
    name: 'Preferences',
    fields: () => ({
        _id: {type: GraphQLID},
        category: {
            type: GraphQLList(CategoryType),
            resolve(parent, args) {
                return Category.find({_id: parent.category});
            }
        },
        user: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({_id: parent.user, deleted_at: null});
            }
        },
        value: {type: GraphQLBoolean},
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
    })
});

const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        _id: {type: GraphQLID},
        firstName: {type: GraphQLString},
        lastName: {type: GraphQLString},
        email: {type: GraphQLString},
        password: {type: GraphQLString},
        nrComments: {
            type: GraphQLInt,
            resolve(parent, args) {
                let comments = Comment.countDocuments({user: parent._id,});
                return comments;
            }
        },
        role: {
            type: RoleType,
            resolve(parent, args) {
                return Role.findById(parent.role);
            }
        },
        karma: {
            type: KarmaType,
            resolve(parent, args) {
                return Karma.findById(parent.karma);
            }
        },
        preferences: {
            type: GraphQLList(PreferencesType),
            resolve(parent, args) {
                return Preferences.find({user: parent._id, value: true})
            }
        },
        receivedBadges: {
            type: GraphQLList(GivenBadgesType),
            resolve(parent, args) {
                return GivenBadge.find({user: parent._id, status: true})
            }
        },
        image: {type: GraphQLString},
        userDescription: {type: GraphQLString},
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
    })
});

const GivenBadgesType = new GraphQLObjectType({
    name: 'GivenBadge',
    fields: () => ({
        _id: {type: GraphQLID},
        //FALTA AQUI O USER TYPE
        user: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({_id: parent.user, deleted_at: null});
            }
        },
        comment: {
            type: GraphQLList(CommentType),
            resolve(parent, args) {
                return Comment.find({_id: parent.comment, deleted_at: null});
            }
        },
        topic: {
            type: GraphQLList(TopicType),
            resolve(parent, args) {
                return Topic.find({_id: parent.topic, deleted_at: null});
            }
        },
        badge: {
            type: GraphQLList(BadgesType),
            resolve(parent, args) {
                return Badge.find({_id: parent.badge, deleted_at: null});
            }
        },
        date_given: {type: GraphQLString},
        attributed_by: {
            type: UserType,
            resolve(parent, args) {
                return User.find({_id: parent.attributed_by, deleted_at: null});
            }
        },
        status: {type: GraphQLBoolean}
    })
});

const TagsType = new GraphQLObjectType({
    name: 'Tag',
    fields: () => ({
        _id: {type: GraphQLID},
        name: {type: GraphQLString},
        category: {
            type: CategoryType,
            resolve(parent, args) {
                return Category.find({_id: parent.category, deleted_at: null});
            }
        },
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
    })
});

const CommunityType = new GraphQLObjectType({
    name: 'Community',
    fields: () => ({
        _id: {type: GraphQLID},
        name: {type: GraphQLString},
        description: {type: GraphQLString},
        number_members: {type: GraphQLInt},
        category: {
            type: GraphQLList(CategoryType),
            resolve(parent, args) {
                return Category.find({_id: parent.category, deleted_at: null});
            }
        },
        exampleMembers: {
            type: GraphQLList(UserCommunityType),
            resolve(parent, args) {
                return UserCommunity.find({community: parent._id, status: true}).limit(3);
            }
        },
        image: {type: GraphQLString},
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
        userRequesting: {type: GraphQLString}
    })
});

const UserCommunityType = new GraphQLObjectType({
    name: 'UserCommunity',
    fields: () => ({
        _id: {type: GraphQLID},
        user: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({_id: parent.user, deleted_at: null})
            }
        },
        community: {
            type: GraphQLList(CommunityType),
            resolve(parent, args) {
                return Community.find({_id: parent.community, deleted_at: null})
            }
        },
        status: {type: GraphQLBoolean},
        role: {
            type: GraphQLList(RoleType),
            resolve(parent, args) {
                return Role.find({_id: parent.role})
            }
        },
        entry_message: {type: GraphQLString},
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
    })
});

const TopicType = new GraphQLObjectType({
    name: 'Topic',
    fields: () => ({
        _id: {type: GraphQLID},
        title: {type: GraphQLString},
        body: {type: GraphQLString},
        user: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({_id: parent.user, deleted_at: null});
            }
        },
        source: {type: GraphQLString},
        metaImage: {type: GraphQLString},
        category: {
            type: GraphQLList(CategoryType),
            resolve(parent, args) {
                return Category.find({_id: parent.category, deleted_at: null});
            }
        },
        tags: {
            type: GraphQLList(TagsType),
            resolve(parent, args) {
                let arrayPush = [];

                function topicCheck() {
                    if (parent.tags[0].id) {
                        for (let i = 0; i < parent.tags.length; i++) {
                            arrayPush.push(parent.tags[i].id);

                        }
                    } else {
                        arrayPush = parent.tags;
                    }
                }

                topicCheck();
                return Tag.find({_id: {$in: arrayPush}, deleted_at: null})
            }
        },
        givenBadges: {
            type: GraphQLList(GivenBadgesType),
            resolve(parent, args) {
                return GivenBadge.find({topic: parent._id})
            }
        },
        upvotes: {type: GraphQLInt},
        nrComments: {type: GraphQLInt},
        main: {type: GraphQLBoolean},
        community: {
            type: CommunityType,
            resolve(parent, args) {
                return Community.find({_id: parent.community, deleted_at: null});
            }
        },
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
        comments: {
            type: GraphQLList(CommentType),
            resolve(parent, args) {
                return Comment.find({topic: parent.id, parent: null, deleted_at: null});
            }
        },
        userRequesting: {type: GraphQLString}
    })
});

const CommentType = new GraphQLObjectType({
    name: 'Comment',
    fields: () => ({
        _id: {type: GraphQLID},
        children: {
            type: GraphQLList(CommentType),
            resolve(parent, args) {
                return Comment.find({parent: parent.id, deleted_at: null});
            },
        },
        parent: {type: GraphQLID},
        user: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({_id: parent.user, deleted_at: null});
            }
        },
        topic: {
            type: GraphQLList(TopicType),
            resolve(parent, args) {
                return Topic.find({_id: parent.topic, deleted_at: null});
            }
        },
        givenBadges: {
            type: GraphQLList(GivenBadgesType),
            resolve(parent, args) {
                return GivenBadge.find({comment: parent._id})
            }
        },
        body: {type: GraphQLString},
        upvotes: {type: GraphQLInt},
        created_at: {type: GraphQLString},
        edited_at: {type: GraphQLString},
        deleted_at: {type: GraphQLString},
    }),
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        // book: {
        //     type: BookType,
        //     args: {id: {type: GraphQLID}},
        //     resolve(parent, args) {
        //         return Book.findById(args.id);
        //     }
        // },
        // books: {
        //     type: GraphQLList(BookType),
        //     resolve(parent, args) {
        //         return Book.find({}).sort({name: -1});
        //     }
        // },
        // author: {
        //     type: AuthorType,
        //     args: {id: {type: GraphQLID}},
        //     resolve(parent, args) {
        //         return Author.findById(args.id);
        //     }
        // },
        // authors: {
        //     type: GraphQLList(AuthorType),
        //     resolve(parent, args) {
        //         return Author.find({});
        //     }
        // },
        comment: {
            type: CommentType,
            args: {_id: {type: GraphQLID}},
            resolve(parent, args) {
                return Comment.findOne({_id: args._id, deleted_at: null});
            }
        },
        getTopicById: {
            type: TopicType,
            args: {
                _id: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                return Topic.findOne({_id: args._id, deleted_at: null});
            }
        },
        getTopicsRecOrCat: {
            //SO FAZ REC
            type: GraphQLList(TopicType),
            args: {
                userRequesting: {type: GraphQLNonNull(GraphQLString)},
                category: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let arrayPush = [];
                if (args.category === 'null') {
                    return client.send(new rqs.RecommendItemsToUser(args.userRequesting, 10,
                        {'filter': "'topic' != NULL"}
                    )).then((result) => {
                        if (result.recomms.length === 0) {
                            return Topic.find({}).limit(5);
                        } else {
                            for (let i = 0; i < result.recomms.length; i++) {
                                arrayPush.push(result.recomms[i].id);
                            }
                            return Topic.find({_id: {$in: arrayPush}, deleted_at: null})
                        }
                    });
                } else {
                    return Topic.find({category: args.category, deleted_at: null})
                }
            }
        },
        getCommunitiesRec: {
            //SO FAZ REC
            type: GraphQLList(CommunityType),
            args: {
                userRequesting: {type: GraphQLNonNull(GraphQLString)},
                category: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let arrayPush = [];
                if (args.category === 'null') {
                    return client.send(new rqs.RecommendItemsToUser(args.userRequesting, 10,
                        {'filter': "'community' != NULL"}
                    )).then((result) => {
                        if (result.recomms.length === 0) {
                            return Community.find({}).limit(5);
                        } else {
                            for (let i = 0; i < result.recomms.length; i++) {
                                arrayPush.push(result.recomms[i].id);
                            }
                            return Community.find({_id: {$in: arrayPush}, deleted_at: null})
                        }
                    });
                } else {
                    return Community.find({category: args.category, deleted_at: null})
                }
            }
        },

        // getTopicsByRec:{
        //   type: TopicRecType,
        //   args:{
        //       userRequesting: {type: GraphQLNonNull(GraphQLString)}
        //   },
        //     resolve(parent,args){
        //
        //     }
        // },

        getBadges: {
            type: GraphQLList(BadgesType),
            resolve(parent, args) {
                return Badge.find({deleted_at: null})
            }
        },
        getBadgeById: {
            type: BadgesType,
            args: {
                _id: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                return Badge.findOne({_id: args._id, deleted_at: null});
            }
        },
        getFavouritesByUser: {
            type: GraphQLList(FavouriteType),
            args: {
                user: {type: GraphQLNonNull(GraphQLID)},
            },
            resolve(parent, args) {
                return Favourite.find({deleted_at: null, user: args.user});
            }
        },
        checkFavourite: {
            type: GraphQLList(FavouriteType),
            args: {
                user: {type: GraphQLNonNull(GraphQLID)},
                topic: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                return Favourite.find({user: args.user, topic: args.topic, deleted_at: null})
            }
        },
        checkUpvote: {
            type: GraphQLList(UpvotesHistoryType),
            args: {
                user: {type: GraphQLNonNull(GraphQLID)},
                topic: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                return UpvotesHistory.find({user_rewarding: args.user, topic: args.topic, upvote: true}).limit(1);
            }
        },

        getTopics: {
            type: GraphQLList(TopicType),
            resolve(parent, args) {
                return Topic.find({deleted_at: null}).limit(10);
            }
        },
        getTopicsByCategory: {
            type: GraphQLList(TopicType),
            args: {
                category: {type: GraphQLNonNull(GraphQLString)},
            },
            resolve(parent, args) {
                if (args.category === 'null') {
                    return Topic.find({deleted_at: null}).sort({created_at: -1}).limit(2)
                } else {
                    return Topic.find({category: args.category, deleted_at: null}).sort({created_at: -1}).limit(2)
                }
            },
        },
        //FOR TESTING, DELETE LATER
        getUsers: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({deleted_at: null}).limit(20);
            }
        },
        getUserByName: {
            type: GraphQLList(UserType),
            args: {firstName: {type: GraphQLNonNull(GraphQLString)}},
            resolve(parent, args) {
                let splitName = args.firstName.split(' ', 2);
                let searchRegexLast1;
                let searchRegexLast2;
                let searchRegexFirst1 = new RegExp(splitName[0], "i");
                if (splitName[1] !== undefined && splitName[1] !== '') {
                    searchRegexLast1 = new RegExp(splitName[1], "i");
                }
                splitName[0] = splitName[0].normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                if (splitName[1] !== undefined && splitName[1] !== '') {
                    splitName[1] = splitName[1].normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                }
                let searchRegexFirst2 = new RegExp(splitName[0], "i");
                if (splitName[1] !== undefined && splitName[1] !== '') {
                    searchRegexLast2 = new RegExp(splitName[1], "i");
                }
                if (splitName[1] !== undefined && splitName[1] !== '') {
                    return User.find({
                        $or: [{firstName: {$regex: searchRegexFirst1}}, {lastName: {$regex: searchRegexLast1}},
                            {firstName: {$regex: searchRegexFirst2}}, {lastName: {$regex: searchRegexLast2}},
                            {firstName: {$regex: searchRegexLast1}}, {lastName: {$regex: searchRegexFirst1}},
                            {firstName: {$regex: searchRegexLast2}}, {lastName: {$regex: searchRegexFirst2}}]
                    }).limit(5);
                } else {
                    return User.find({
                        $or: [{firstName: {$regex: searchRegexFirst1}},
                            {firstName: {$regex: searchRegexFirst2}},
                            {lastName: {$regex: searchRegexFirst1}},
                            {lastName: {$regex: searchRegexFirst2}}]
                    }).limit(5);
                }
            }
        },
        getUserById: {
            type: UserType,
            args: {
                _id: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                return User.findById(args._id);
            }
        },
        getCategories: {
            type: GraphQLList(CategoryType),
            resolve(parent, args) {
                return Category.find({deleted_at: null})
            }
        },
        getCommentsByUser: {
            type: GraphQLList(CommentType),
            args: {user: {type: GraphQLNonNull(GraphQLID)}},
            resolve(parent, args) {
                return Comment.find({user: args.user, deleted_at: null});
            }
        },
        getCommunityById: {
            type: GraphQLList(CommunityType),
            args: {_id: {type: GraphQLNonNull(GraphQLID)}},
            resolve(parent, args) {
                return Community.find({_id: args._id, deleted_at: null})
            }
        },
        getConversationById: {
            type: GraphQLList(ConversationType),
            args: {_id: {type: GraphQLNonNull(GraphQLID)}},
            resolve(parent, args) {
                return Conversation.find({_id: args._id});
            }
        },
        getConversationsByUser: {
            type: GraphQLList(ConversationType),
            args: {user_1: {type: GraphQLNonNull(GraphQLID)}},
            resolve(parent, args) {
                return Conversation.find({$or: [{user_1: args.user_1}, {user_2: args.user_1}]}).sort({created_at: -1});
            }
        },
        getConversationsWithUser: {
            type: GraphQLList(ConversationType),
            args: {
                user_1: {type: GraphQLNonNull(GraphQLID)},
                user_2: {type: GraphQLNonNull(GraphQLID)},
            },
            resolve(parent, args) {
                return Conversation.find({
                    $or: [
                        {user_1: args.user_1, user_2: args.user_2},
                        {user_1: args.user_2, user_2: args.user_1}
                    ]
                });
            }
        },
        getCommunities: {
            type: GraphQLList(CommunityType),
            resolve(parent, args) {
                return Community.find({}).limit(20);
            }
        },
        getCommunitiesByUser: {
            type: GraphQLList(UserCommunityType),
            args: {
                user: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                return UserCommunity.find({user: args.user, status: true})
            }
        },
        getMessagesByConversation: {
            type: GraphQLList(MessageType),
            args: {conversation: {type: GraphQLNonNull(GraphQLID)}, skip: {type: GraphQLNonNull(GraphQLInt)}},
            resolve(parent, args) {
                return Message.find({conversation: args.conversation, deleted_at: null}).limit(30).skip(args.skip);
            }
        },
        checkUserOnCommunity: {
            type: GraphQLList(UserCommunityType),
            args: {
                user: {type: GraphQLNonNull(GraphQLID)},
                community: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                return UserCommunity.find({
                    user: args.user,
                    community: args.community,
                    deleted_at: null
                }).sort({created_at: -1}).limit(1);
            }
        },
        getPendingOnCommunity: {
            type: GraphQLList(UserCommunityType),
            args: {
                community: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                return UserCommunity.find({community: args.community, status: null})
            }
        },
        getTagsBySearch: {
            type: GraphQLList(TagsType),
            args: {
                name: {type: GraphQLNonNull(GraphQLString)},
            },
            resolve(parent, args) {
                let splitName = args.name.split(' ', 2);
                splitName[0] = new RegExp(splitName[0], "i");
                return Tag.find({name: {$regex: splitName[0]}, deleted_at: null})
            }
        },
        getCategoriesBySearch: {
            type: GraphQLList(CategoryType),
            args: {
                name: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let splitName = args.name.split(' ', 2);
                splitName[0] = new RegExp(splitName[0], "i");
                return Category.find({name: {$regex: splitName[0]}, deleted_at: null})
            }
        },
        getTopicsBySearch: {
            type: GraphQLList(TopicType),
            args: {
                title: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let splitTitle = args.title.split(' ', 2);
                splitTitle[0] = new RegExp(splitTitle[0], "i");
                return Topic.find({title: {$regex: splitTitle[0]}, deleted_at: null}).limit(4);
            }
        },
        addUserSawTopic: {
            type: GraphQLList(TopicRecType),
            args: {
                user: {type: GraphQLNonNull(GraphQLID)},
                topic: {type: GraphQLNonNull(GraphQLID)}
            },
            resolve(parent, args) {
                addDetail(args.user, args.topic);
                return Topic.find({_id: args.topic});
            }
        },
        getCommunitiesBySearch: {
            type: GraphQLList(CommunityType),
            args: {
                name: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let splitName = args.name.split(' ', 2);
                splitName[0] = new RegExp(splitName[0], "i");
                return Community.find({name: {$regex: splitName[0]}, deleted_at: null}).limit(4)
            }
        },
        getUsersBySearch: {
            type: GraphQLList(UserType),
            args: {
                firstName: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let splitName = args.firstName.split(' ', 2);
                let searchRegexLast1;
                let searchRegexLast2;
                let searchRegexFirst1 = new RegExp(splitName[0], "i");
                if (splitName[1] !== undefined && splitName[1] !== '') {
                    searchRegexLast1 = new RegExp(splitName[1], "i");
                }
                splitName[0] = splitName[0].normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                if (splitName[1] !== undefined && splitName[1] !== '') {
                    splitName[1] = splitName[1].normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                }
                let searchRegexFirst2 = new RegExp(splitName[0], "i");
                if (splitName[1] !== undefined && splitName[1] !== '') {
                    searchRegexLast2 = new RegExp(splitName[1], "i");
                }
                if (splitName[1] !== undefined && splitName[1] !== '') {
                    return User.find({
                        $or: [{firstName: {$regex: searchRegexFirst1}}, {lastName: {$regex: searchRegexLast1}},
                            {firstName: {$regex: searchRegexFirst2}}, {lastName: {$regex: searchRegexLast2}},
                            {firstName: {$regex: searchRegexLast1}}, {lastName: {$regex: searchRegexFirst1}},
                            {firstName: {$regex: searchRegexLast2}}, {lastName: {$regex: searchRegexFirst2}}]
                    }).limit(5);
                } else {
                    return User.find({
                        $or: [{firstName: {$regex: searchRegexFirst1}},
                            {firstName: {$regex: searchRegexFirst2}},
                            {lastName: {$regex: searchRegexFirst1}},
                            {lastName: {$regex: searchRegexFirst2}}]
                    }).limit(5);
                }
            }
        },
    }
});

const Mutation = new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            addComment: {
                type: CommentType,
                args: {
                    parent: {type: GraphQLID},
                    topic: {type: GraphQLNonNull(GraphQLID)},
                    body: {type: GraphQLNonNull(GraphQLString)},
                    user: {type: GraphQLNonNull(GraphQLID)}
                },
                resolve(parent, args) {
                    let parentID = args.parent;
                    if (!args.parent) parentID = null;
                    let comment = new Comment({
                        parent: parentID,
                        topic: args.topic,
                        user: args.user,
                        body: args.body,
                    });
                    return comment.save().then(() => {
                        return Topic.findById(args.topic).then((result) => {
                            return Topic.updateOne(
                                {_id: result._id},
                                {nrComments: result.nrComments + 1}).then(() => {
                                return comment
                            })
                        })
                    });
                }
            },
            addCategory: {
                type: CategoryType,
                args: {
                    name: {type: GraphQLNonNull(GraphQLString)},
                    description: {type: GraphQLNonNull(GraphQLString)},
                    image: {type: GraphQLString},
                },
                resolve(parent, args) {
                    let category = new Category({
                        name: args.name,
                        description: args.description,
                        image: args.image
                    });
                    return category.save();
                }
            },
            addFavourite: {
                type: FavouriteType,
                args: {
                    user: {type: GraphQLNonNull(GraphQLID)},
                    topic: {type: GraphQLNonNull(GraphQLID)}
                },
                resolve(parent, args) {
                    let favourite = new Favourite({
                        user: args.user,
                        topic: args.topic
                    });
                    return favourite.save();
                }
            },
            deleteFavourite: {
                type: FavouriteType,
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)}
                },
                resolve(parent, args) {
                    return Favourite.updateOne(
                        {_id: args._id},
                        {deleted_at: Date.now()}
                    ).then(() => {
                        return Favourite.findById(args._id)
                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
            addRole: {
                type: RoleType,
                args: {
                    name: {type: GraphQLNonNull(GraphQLString)},
                    description: {type: GraphQLNonNull(GraphQLString)}
                },
                resolve(parent, args) {
                    let role = new Role({
                        name: args.name,
                        description: args.description
                    });
                    return role.save();
                }
            },
            addBadge: {
                type: BadgesType,
                args: {
                    name: {type: GraphQLNonNull(GraphQLString)},
                    description: {type: GraphQLNonNull(GraphQLString)},
                    image: {type: GraphQLString}
                },
                resolve(parent, args) {
                    let badge = new Badge({
                        name: args.name,
                        description: args.description,
                        image: args.image
                    });
                    return badge.save();
                }
            },
            addCommunity: {
                type: CommunityType,
                args: {
                    name: {type: GraphQLNonNull(GraphQLString)},
                    description: {type: GraphQLNonNull(GraphQLString)},
                    category: {type: GraphQLNonNull(GraphQLID)},
                    image: {type: GraphQLNonNull(GraphQLString)}
                },
                resolve(parent, args) {
                    let community = new Community(args);
                    return community.save().then((res) => {
                        addItem(res._id, {
                            'community': true,
                            'category': args.category
                        });
                        return community
                    });
                }
            },
            addAdminCommunity: {
                type: UserCommunityInput,
                args: {
                    user: {type: GraphQLNonNull(GraphQLID)},
                    community: {type: GraphQLNonNull(GraphQLID)},
                    entry_message: {type: GraphQLNonNull(GraphQLString)},
                },
                resolve(parent, args) {
                    args.role = "5e336102cf4d187e1597f27e";
                    args.status = true;
                    let usercommunity = new UserCommunity(args);
                    return usercommunity.save();
                }
            },
            addUserCommunity: {
                type: UserCommunityInput,
                args: {
                    user: {type: GraphQLNonNull(GraphQLID)},
                    community: {type: GraphQLNonNull(GraphQLID)},
                    entry_message: {type: GraphQLNonNull(GraphQLString)},
                },
                resolve(parent, args) {
                    let usercommunity = new UserCommunity(args);
                    return usercommunity.save();
                }
            },
            addGivenBadge: {
                type: GivenBadgesInput,
                args: {
                    user: {type: GraphQLNonNull(GraphQLID)},
                    comment: {type: GraphQLID},
                    topic: {type: GraphQLID},
                    badge: {type: GraphQLNonNull(GraphQLID)},
                    attributed_by: {type: GraphQLNonNull(GraphQLID)},
                },
                resolve(parent, args) {
                    let givenBadge = new GivenBadge(
                        args
                    );
                    return givenBadge.save()
                }
            },
            addKarma: {
                type: KarmaType,
                args: {
                    topics: {type: GraphQLInt},
                    comments: {type: GraphQLInt}
                },
                resolve(parent, args) {
                    let karma = new Karma({
                        topics: args.topics,
                        comments: args.comments
                    });
                    return karma.save();
                }
            },
            addTag: {
                type: TagsType,
                args: {
                    name: {type: GraphQLNonNull(GraphQLString)},
                    category: {type: GraphQLNonNull(GraphQLString)}
                },
                resolve(parent, args) {
                    let tag = new Tag(args);
                    return tag.save();
                }
            },
            addMessage: {
                type: MessageType,
                args: {
                    from: {type: GraphQLNonNull(GraphQLID)},
                    to: {type: GraphQLNonNull(GraphQLID)},
                    body: {type: GraphQLNonNull(GraphQLString)},
                    conversation: {type: GraphQLNonNull(GraphQLID)}
                },
                resolve(parent, args) {
                    let message = new Message(args);
                    return message.save();
                }
            },
            addConversation: {
                type: ConversationType,
                args: {
                    user_1: {type: GraphQLNonNull(GraphQLID)},
                    user_2: {type: GraphQLNonNull(GraphQLID)},
                },
                resolve(parent, args) {
                    let toReturn = [];

                    function checkConversation() {
                        return new Promise(function (resolve, reject) {
                            Conversation.find({user_1: args.user_1, user_2: args.user_2}).then((result) => {
                                if (result.length > 0) {
                                    toReturn = result[0];
                                    resolve(toReturn);
                                } else {
                                    Conversation.find({user_1: args.user_2, user_2: args.user_1}).then((result) => {
                                        if (result.length > 0) {
                                            toReturn = result[0];
                                            resolve(toReturn);
                                        } else {
                                            let conversation = new Conversation(args);
                                            toReturn = conversation.save();
                                            resolve(toReturn);
                                        }
                                    }).catch((err) => {
                                        return reject(err)
                                    })
                                }
                            }).catch((err) => {
                                return reject(err)
                            })
                        })
                    }

                    async function callCheck() {
                        await checkConversation().then(() => {
                            return toReturn;
                        })
                    }

                    return callCheck().then(() => {
                        return toReturn
                    })
                }
            },
            addPreference: {
                type: PreferencesType,
                args: {
                    category: {type: GraphQLNonNull(GraphQLID)},
                    user: {type: GraphQLNonNull(GraphQLID)}
                },
                resolve(parent, args) {
                    args.value = true;
                    let id;
                    let preferences = new Preferences(args);
                    return preferences.save().then((result) => {
                        setUserValues(args.user, {
                            [result.category]: true
                        });
                        return preferences
                    })
                }
            },
            addUpvote: {
                type: UpvotesHistoryType,
                args: {
                    topic: {type: GraphQLID},
                    comment: {type: GraphQLID},
                    user_rewarded: {type: GraphQLNonNull(GraphQLID)},
                    user_rewarding: {type: GraphQLNonNull(GraphQLID)},
                },
                resolve(parents, args) {
                    let upvote = new UpvotesHistory(args);
                    return upvote.save().then(() => {
                        if (args.topic) {
                            return Topic.findById(args.topic).then((result) => {
                                return Topic.updateOne(
                                    {_id: result._id},
                                    {upvotes: result.upvotes + 1}
                                ).then(() => {
                                    return Topic.findById(args.topic).then(() => {
                                        return User.findById(args.user_rewarded).then((res) => {
                                            return Karma.findById(res.karma).then((res2) => {
                                                return Karma.updateOne(
                                                    {_id: res2._id},
                                                    {topics: res2.topics + 1}
                                                ).then((resul) => {
                                                    console.log(resul)
                                                })
                                            })
                                        })
                                    })
                                }).catch((err) => {
                                    return new GraphQLError(
                                        err
                                    )
                                })
                            });
                        }
                    })
                }
            },
            deleteUpvote: {
                type: GraphQLList(UpvotesHistoryType),
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)}
                },
                resolve(parent, args) {
                    return UpvotesHistory.updateOne(
                        {_id: args._id},
                        {upvote: false}
                    ).then(() => {
                        return UpvotesHistory.findById(args._id).then((res) => {
                            if (res.topic !== null) {
                                return Topic.findById(res.topic).then((result) => {
                                    return Topic.updateOne(
                                        {_id: result._id},
                                        {upvotes: result.upvotes - 1}
                                    ).then(() => {
                                        return User.findById(result.user).then((result2) => {
                                            return Karma.findById(result2.karma).then((result3) => {
                                                return Karma.updateOne(
                                                    {_id: result3._id},
                                                    {topics: result3.topics - 1}
                                                ).then((resul) => {
                                                    console.log(resul)
                                                })
                                            })
                                        })
                                    }).catch((err) => {
                                        return new GraphQLError(
                                            err
                                        )
                                    })
                                });
                            }
                        })
                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
            addTopic: {
                type: TopicTypeInput,
                args: {
                    user: {type: GraphQLNonNull(GraphQLID)},
                    title: {type: GraphQLNonNull(GraphQLString)},
                    body: {type: GraphQLNonNull(GraphQLString)},
                    source: {type: GraphQLString},
                    metaImage: {type: GraphQLNonNull(GraphQLString)},
                    category: {type: GraphQLNonNull(GraphQLString)},
                    tags: {type: GraphQLList(TopicTagInput)},
                    main: {type: GraphQLBoolean},
                    community: {type: GraphQLString}
                },
                resolve(parent, args) {
                    let arrayTags = [];

                    function tagFunction(tagName, arrayTags, category) {
                        return new Promise(function (resolve, reject) {
                            Tag.find({name: tagName.toUpperCase()}).then((result) => {
                                if (result.length > 0) {
                                    let pass = true;
                                    for (let i = 0; i < arrayTags.length; i++) {
                                        if (arrayTags.length > 0) {
                                            if (arrayTags[i].name === tagName.toUpperCase()) {
                                                pass = false;
                                                return null;
                                            }
                                        }
                                    }
                                    if (pass) {
                                        arrayTags.push({_id: result[0]._id.toString(), name: tagName.toUpperCase()});
                                        resolve(arrayTags);
                                    }
                                } else {
                                    let tag = new Tag({name: tagName.toUpperCase(), category: category});
                                    tag.save().then((result) => {
                                        arrayTags.push({_id: result._id.toString(), name: tagName.toUpperCase()});
                                        resolve(arrayTags);
                                    })
                                }
                            }).catch((err) => {
                                reject(new Error(err));
                            })
                        })
                    }

                    async function loopTags() {
                        for (let i = 0; i < args.tags.length; i++) {
                            arrayTags = await tagFunction(args.tags[i].name, arrayTags, args.category);
                        }
                        return arrayTags;
                    }

                    return loopTags().then((result) => {
                        args.tags = result;
                        let topic = new Topic(args);
                        return topic.save().then((res) => {
                            addItem(res._id, {
                                'topic': true,
                                'category': res.category
                            });
                            return topic
                        });
                    });

                }
            },
            updateTopic: {
                type: TopicType,
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)},
                    title: {type: GraphQLString},
                    body: {type: GraphQLString},
                    source: {type: GraphQLString},
                    metaImage: {type: GraphQLString},
                    category: {type: GraphQLString},
                    tags: {type: GraphQLList(TopicTagInput)},
                    community: {type: GraphQLString}
                },
                resolve(parent, args) {
                    args.edited_at = Date.now();
                    return Topic.updateOne(
                        {_id: args._id},
                        args
                    ).then(() => {
                        return Topic.findById(args._id)
                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
            updateComment: {
                type: CommentType,
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)},
                    body: {type: GraphQLID},
                },
                resolve(parent, args) {
                    args.edited_at = Date.now();
                    return Comment.updateOne(
                        {_id: args._id},
                        args
                    ).then(() => {
                        return Comment.findById(args._id);
                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
            updateCategory: {
                type: CategoryType,
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)},
                    name: {type: GraphQLString},
                    description: {type: GraphQLString},
                    image: {type: GraphQLString}
                },
                resolve(parent, args) {
                    args.edited_at = Date.now();
                    return Category.updateOne(
                        {_id: args._id},
                        args
                    ).then(() => {
                        return Category.findById(args._id)
                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
            deleteTopic: {
                type: TopicType,
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)},
                },
                resolve(parent, args) {
                    return Topic.updateOne(
                        {_id: args._id},
                        {deleted_at: Date.now()}
                    ).then(() => {
                        return Topic.findById(args._id)
                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
            deleteComment: {
                type: CommentType,
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)}
                },
                resolve(parent, args) {
                    return Comment.updateOne(
                        {_id: args._id},
                        {deleted_at: Date.now()}
                    ).then(() => {
                        return Comment.findById(args._id);
                        //SUBSTITUIR POR MENSAGEM DE DELETE
                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
            deleteCategory: {
                type: CategoryType,
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)},
                },
                resolve(parent, args) {
                    return Category.updateOne(
                        {_id: args._id},
                        {deleted_at: Date.now()}
                    ).then(() => {
                        return Category.findById(args._id);
                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
            addPendingUserOnCommunity: {
                type: UserCommunityInput,
                args: {
                    user: {type: GraphQLNonNull(GraphQLID)},
                    community: {type: GraphQLNonNull(GraphQLID)},
                    entry_message: {type: GraphQLNonNull(GraphQLString)},
                },
                resolve(parent, args) {
                    let useroncommunity = new UserCommunity(args);
                    return useroncommunity.save();
                }
            },
            updatePendingUserOnCommunity: {
                type: UserCommunityInput,
                args: {
                    _id: {type: GraphQLNonNull(GraphQLID)},
                    status: {type: GraphQLNonNull(GraphQLBoolean)}
                },
                resolve(parent, args) {
                    args.edited_at = Date.now();
                    if (args.status === false) args.deleted_at = Date.now();
                    return UserCommunity.updateOne(
                        {_id: args._id},
                        args
                    ).then(() => {
                        return UserCommunity.findById(args._id).then((result) => {
                            return Community.findById(result.community).then((res) => {
                                if (args.status === true) {
                                    return Community.updateOne(
                                        {_id: result.community},
                                        {number_members: res.number_members + 1}
                                    ).then(() => {
                                    }).catch((err) => {
                                        console.log(err)
                                    })
                                }
                            })
                        })

                    }).catch((err) => {
                        return new GraphQLError(
                            err
                        )
                    })
                }
            },
        }
    })
;


module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});




