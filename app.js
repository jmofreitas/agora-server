const express = require('express');
const config = require('config');
const graphqlHTTP = require('express-graphql');
const users = require('./routes/users');
const auth = require('./routes/auth');
const verifyToken = require('./routes/verifyToken');
const schema = require('./schema/schema');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const rec = require('./recommendations/rec.js');
const jwt = require('jsonwebtoken');
const app = express();

if (!config.get('PrivateKey')) {
    console.error('FATAL ERROR: PrivateKey is not defined.');
    process.exit(1);
}

// allow cross-origin req
app.use(cors());

mongoose.connect("mongodb+srv://ad-user:2Fd13r0Zd8wgPEqV@cluster0-kovcz.mongodb.net/test?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    },
    // function (err, db){ {
    // console.log(result)
// })}
);

mongoose.connection.once('open', () => {
    // users.createIndex({firstName: "text", lastName: "text"});
    console.log('connected to database')
});

app.use(bodyParser.json());
//
// app.use('/graphql', graphqlHTTP({
//     schema,
//     graphiql: true
// }));

app.use('/graphql', (req, res, next) => {
    const token = req.headers['authorization'];
    try {
        jwt.verify(token, config.get('PrivateKey'));
        next();
    } catch (e) {
        res.status(401).json({
            message: e.message
        })
    }
}, graphqlHTTP({
    schema,
    graphiql: true
}));

app.use('/user', users);
app.use('/auth', auth);
app.use('/verifyToken', verifyToken);


//RECOMMENDATIONS

// app.use('/topictouser/:idUser/:number', function(req, res, next){
//  rec.recommendTopicToUser(req.params.idUser, req.params.number)
// });

//UPLOADS
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        console.log(file);
        cb(null, file.originalname)
    }
});


app.post('/upload', (req, res, next) => {
    const upload = multer({storage}).single('imgFile');
    upload(req, res, function (err) {
        if (err) {
            return res.send(err)
        }
        console.log('file uploaded to server');
        console.log(req.file);
        console.log(req.body);

        // SEND FILE TO CLOUDINARY
        const cloudinary = require('cloudinary').v2;
        cloudinary.config({
            cloud_name: 'agoraproject',
            api_key: '271948946383987',
            api_secret: '5sNKqPszSPTIt70fHv8m4xH5Aps'
        });

        const path = req.file.path;
        const type = req.body.type;
        const uniqueFilename = new Date().toISOString();
        if (type === undefined) return res.send({error: 'Type is not defined'});
        cloudinary.uploader.upload(
            path,
            {public_id: `${type}/uploads/${uniqueFilename}`, tags: [`teste_upload`, `teste upload2`]}, // directory and tags are optional
            function (err, image) {
                if (err) return res.send(err);
                // remove file from server
                const fs = require('fs');
                fs.unlinkSync(path);
                // return image details
                res.json(image);
            }
        )
    })
});


app.listen(4000, () => {
    console.log('now listening for requests on port 4000');
});