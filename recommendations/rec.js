const recombee = require('recombee-api-client');
const rqs = recombee.requests;

const client = new recombee.ApiClient('projectagora-prod', 'qG5yaOZODkdZisTt0KHio1VT1SUqLEcjjya3II4gFTUTLGurBuRLQiqPBvM6vAvX');

const recommendToUser = (userID, number, type) => {
    let rec = [];
    if (type === 'topic') {
        client.send(new rqs.RecommendItemsToUser(userID, number,
            {'filter': "'topic' != NULL"}
        )).then((result) => {
            console.log(result);
            rec = result.recomms;
        })
    } else if (type === 'community') {
        client.send(new rqs.RecommendItemsToUser(userID, number,
            {'filter': "'community'"}
        )).then((result) => {
            return result;
        })
    }
    setTimeout(() => {
        return rec;
    }, 8000)
};

const addDetail = (userID, typeID) => {
    client.send(new rqs.AddDetailView(userID, typeID)).then((result) => {
        console.log(result, 'EWQKOEKEQKOQE')
    })
};

const addItem = (itemID, value) => {
    client.send(new rqs.AddItem(itemID)).then((res) => {
        console.log(res);
        client.send(new rqs.SetItemValues(itemID, value)).then((resu) => {
            console.log(resu);
        })
    });
};

const addUser = (userID) => {
    client.send(new rqs.AddUser(userID)).then((result) => {
        console.log(result);
    });
};

const addUserProperty = (propertyName, type) => {
    client.send(new rqs.AddUserProperty(propertyName, type)).then((result) => {
        return console.log(result);
    })
};

const setUserValues = (userID, values) => {
    client.send(new rqs.SetUserValues(userID, values)).then((result) => {
        return console.log(result);
    })
};

module.exports = {
    recommendToUser,
    addUser,
    addUserProperty,
    setUserValues,
    addItem,
    addDetail,
};
