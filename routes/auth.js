const jwt = require('jsonwebtoken');
const config = require('config');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const User = require('../models/user').User;
const express = require('express');
const router = express.Router();

router.post('/', async (req, res) => {

    const {error} = validate(req.body);

    const auth = Buffer.from(req.headers.authorization.split(" ")[1], 'base64').toString();
    const email = auth.split(":")[0];
    const password = auth.split(":")[1];

    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    let user = await User.findOne({email: email});

    if (!user) {
        return res.status(400).send('Email ou password incorreto!');
    }

    const validPassword = await bcrypt.compare(password, user.password);

    if (!validPassword) {
        return res.status(400).send('Email ou password incorreto!');
    }
    const token = jwt.sign({
            _id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            image: user.image
        },
        config.get('PrivateKey'),
        {expiresIn: '1d'});
    res.header("Access-Control-Allow-Headers", "*"); //VER SE DA PARA DAR DELETE
    res.header("Access-Control-Expose-Headers", "x-auth-token");
    res.header('x-auth-token', token).send(_.pick(user, ['_id', 'firstName', 'lastName', 'email', 'preferences']));
});

function validate(req) {
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()
    };

    return Joi.validate(req, schema);
}


module.exports = router;