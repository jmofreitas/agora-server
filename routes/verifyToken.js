const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const User = require('../models/user').User;
const express = require('express');
const router = express.Router();

router.post('/', async (req, res) => {
    let token = '';
    try {
        token = jwt.verify(req.body.data.token, config.get('PrivateKey'));
        console.log(token);
        res.status(200).send(token);
    } catch (err) {
        console.log(err);
        res.status(401).send(err);
    }

    // const sendToken = jwt.sign({
    //         _id: token._id,
    //         firstName: token.firstName,
    //         lastName: token.lastName,
    //         image: token.image,
    //         email: token.email,
    //         preferences: token.preferences
    //     },
    //     config.get('PrivateKey'),
    //     {expiresIn: '15'}
    // );
});


module.exports = router;