const bcrypt = require('bcrypt');
const _ = require('lodash');
const {validate} = require('../models/user');
const User = require('../models/user').User;
const Karma = require('../models/karma');
const express = require('express');
const router = express.Router();

const addUser = require('../recommendations/rec.js').addUser;

router.post('/', async (req, res) => {
    let body = req.body;

    const auth = Buffer.from(req.headers.authorization.split(" ")[1], 'base64').toString();
    const email = auth.split(":")[0];
    const password = auth.split(":")[1];
    body.email = email;
    body.password = password;

    const {error} = validate(body);

    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    let user = await User.findOne({email: body.email});

    if (user) {
        return res.status(400).send('O email que tentou registar já se encontra em uso.');
    } else {

        user = new User(_.pick(body, ['firstName', 'lastName', 'email', 'password', 'image']));
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
        let karma = new Karma();
        await karma.save().then((result) => {
            user.karma = result._id;
            user.save().then(() => {
                addUser(user._id);
            }).catch((err) => {res.send(err)});
        }).catch((err) => {res.send(err)});
        res.send(_.pick(user, ['_id', 'firstName', 'lastName', 'email', 'image']));
    }
});

module.exports = router;